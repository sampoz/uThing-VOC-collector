#! /bin/bash

# For sanity
set -eo pipefail

cat /dev/ttyACM0 |ruby -ne 'require "./uploader.rb"; Uploader::upload($_)';
