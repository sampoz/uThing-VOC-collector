# uThing::VOC collector to store data to influxdb

See the [device](https://www.tindie.com/products/damancuso/uthingvoc-air-quality-usb-dongle/) for specs.

HOX, This is not ready for production. If you need a production-grade system, see [this tutorial](https://www.hackster.io/damancuso/indoor-air-quality-monitor-b181e9).

# Installation
run `./install.sh`

# Dependencies
Ruby

# TODO
* Setup authentication
* Template address
* Capture all fields