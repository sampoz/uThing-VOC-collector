#! /bin/bash

# For sanity
set -eo pipefail

echo "Copy unit to system"
sudo cp uthing-voc-data-collector.service /etc/systemd/system/

echo "Reloading systemd units"
sudo systemctl daemon-reload

echo "Enabling & starting the service"
sudo systemctl enable uthing-voc-data-collector
sudo service uthing-voc-data-collector start
